#!/usr/bin/env python

import os

mutations = ['KCNQ1_WT+KCNE1', 'KCNQ1_WT50%+KCNE1', 'xKCQN1+KCNE1', 'G119R+WT+KCNE1', 'delF166+WT+KCNE1', 'delG186L187+WT+KCNE1', 'V254L+WT+KCNE1', 'L273V+WT+KCNE1', 'R539L+WT+KCNE1', 'K421E+WT+KCNE1', 'G430fs*28+WT+KCNE1', 'R591C+WT+KCNE1']

for mutation in mutations:
	os.system("../run.py --mutation=%s --overwrite-behaviour=overwrite --imp=OHara"%mutation)
	os.system("../run.py --mutation=%s --overwrite-behaviour=overwrite --imp=OHara --only-vHalf"%mutation)
	os.system("./run.py --mutation=%s --overwrite-behaviour=overwrite --imp=tenTusscherPanfilov"%mutation)
	os.system("../run.py --mutation=%s --overwrite-behaviour=overwrite --imp=tenTusscherPanfilov --only-vHalf"%mutation)
