#!/usr/bin/env python

import numpy as np
from carputils import settings
from carputils import tools
import shutil
import re
import os
import glob

mutations = ['KCNQ1_WT+KCNE1', 'KCNQ1_WT50%+KCNE1', 'xKCQN1+KCNE1', 'G119R+WT+KCNE1', 'delF166+WT+KCNE1', 'delG186L187+WT+KCNE1', 'V254L+WT+KCNE1', 'L273V+WT+KCNE1', 'R539L+WT+KCNE1', 'K421E+WT+KCNE1', 'G430fs*28+WT+KCNE1', 'R591C+WT+KCNE1']
shifts_v = [0, 1.2400, 0.2366, -6.0120, 1.8900, 9.1900, 4.7867, 19.4400, 13.2000, -7.8050, -1.4567, -4.1460]
factorGKs = [1.0, 0.64, 0.53, 0.74, 0.84, 0.63, 0.58, 1.0, 0.84, 0.84, 0.7, 0.72]

def parser():
    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')
    group.add_argument('--mutation',
                        default = 'KCNQ1_WT+KCNE1',
                        choices = mutations,
                        help = 'pick variant')
    group.add_argument('--imp',
                        default = 'OHara',
                        choices = ['OHara', 'tenTusscherPanfilov'],
                        help = 'pick baseline model')    
    group.add_argument('--duration', default='500000',
                        help = 'pick duration of experiment')
    group.add_argument('--bcl',
                        default='500',
                        help = 'pick basic cycle length')
    group.add_argument('--only-vHalf',
                        action = 'store_true')
    return parser


# define job ID
def jobID(args):
    """
    Generate name of top level output directory.
    """
    tpl = './results/SingleCell/exp_{}_{}_bcl_{}_ms_duration_{}_ms'
    if args.only_vHalf:
        tpl += '_onlyVhalf'
    return tpl.format(args.imp, args.mutation, args.bcl, args.duration)
   

@tools.carpexample(parser, jobID, clean_pattern='exp*')
def run(args, job):

    # run bench with available ionic models
    cmd  = ['--duration', args.duration,
            '--stim-assign',    
            '--stim-species', 'Ki']
    
    # configure EP model
    cmd += ['--imp', args.imp]
    iMut = mutations.index(args.mutation)
    if args.imp == "OHara":
        vHalf = 11.6
    elif args.imp == "tenTusscherPanfilov":
        vHalf = 5.
    else:
        sys.exit("Unsupported ionic model: " + args.imp)
    impPar = 'vHalfXs={}'.format(vHalf-shifts_v[iMut])
    if not args.only_vHalf:
        impPar += ',GKs*{}'.format(factorGKs[iMut])
    cmd += ['--imp-par', impPar]

    # setup stimulus
    cmd += ['--stim-curr', 30.0,
            '--numstim', int(float(args.duration)/float(args.bcl)+1),
            '--start-out', (int(float(args.duration)/float(args.bcl)+1)-5)*float(args.bcl),
            '--bcl', args.bcl ]

    # numerical settings
    cmd += ['--dt', 10.0e-3]

    # IO and state management
    expID = '{}_'.format(job.ID)
    if args.ID != '':
        expID = '{}_'.format(args.ID)

    cmd += ['--dt-out', 0.1]

    # Output options
    cmd += ['--fout', '{}'.format(os.path.join(job.ID, args.mutation)),
            '--APstatistics', '--validate']

    job.bench(cmd, msg='Testing {}'.format(args.mutation))
    tools.simfile_path(os.path.join(os.path.dirname(__file__), 'external', 'results/SingleCell/exp_tenTusscherPanfilov_delF166+WT+KCNE1_bcl_500_ms_duration_500000_ms'), mesh=False)

    # move results to folder with job.ID name
    for file in glob.glob(r'*.txt'):
        shutil.move(os.path.join(file), os.path.join(job.ID, file))
    for file in glob.glob(r'*.dat'):
        shutil.move(os.path.join(file), os.path.join(job.ID, file))
    for file in glob.glob(r'*.sv'):
        shutil.move(os.path.join(file), os.path.join(job.ID, file))
    for file in glob.glob(r'BENCH_REG*'):
        shutil.move(os.path.join(file), os.path.join(job.ID, file))

if __name__ == '__main__':
    run()